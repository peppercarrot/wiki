# Creatures

## Creatures of Hereva 

Hereva has many different creatures roaming through the various lands. Dragons are the oldest of these creatures and predate the known history of Hereva. Many various creatures roam Hereva, and many more are being discovered after the Great Tree of Komona's explosion ripped the magical fabric of Hereva apart. This has kept a number of scientists busy trying to classify the previously unknown species. To date there are many strange combinations of creatures ranging from the fearsome Phanda to the peaceful Dragon-Cow. Expeditions into Hereva's forests and uncharted lands are known to bring new discoveries without fail. Some scientific communities have derided these as "phishing and pherreting expeditions", so named for the famous scientist Dr. Pomme who uncovered a pheasant / fish and a pheasant / ferret combination in the same day. "Dr. Pomme just picks the low-hanging fruit to document" they claim. Dr. Pomme dismisses these assertions as petty, claiming that even the modest discoveries add value to the scientific literature. Dr. Pomme has planned many daily expeditions to find new creatures. His graduate students just call these expeditions "Pommes". The graduate students find they can get more work done while he's out of the office, even though his expeditions bring more discoveries that they have to catalog and manage. They don't mind, though, because his micro-managing makes their jobs more difficult. The relieved grad students have a saying: "A Pomme a day keeps the doctor away". 

Listed below are a sampling of Dr. Pomme's discoveries, one from each volume of his 80 volume (and counting) work. These are dedicated to the tireless graduate students who have carefully documented, cataloged, and typeset all 80 (sorry, 81) volumes of this work. It is truly a testament to Dr. Pomme's research and their efforts that these 82 volumes are not only useful to the scientific community, but also a pleasure to read. We can't thank them enough for the quality of this 83 volume text.

## Bestiary

### Argiope Spiders

![Argiope](https://www.peppercarrot.com/data/wiki/medias/img/creature_argiope.jpg)
* **Type**: Arachnid
* **Place**: Usually around the red berry trees in The Haunted Jungle.
* **Episodes**: ep03
* **Description**: Argiope spiders are yellow spiders that are primarily found in The Haunted Jungle. Their most distinguishing feature is a grayish spiral on their body, either on the thorax or the abdomen. They are one of the few spiders in Hereva that hunt in packs, using an intricate system of webbing, signals, and gestures for capturing prey. They also have a remarkably well-developed hierarchy that is still being studied by Hereva's scientists. What can seem like a small batch of curious Argiope spiders might actually be scouting parties keeping watch over their territory and notifying the others as needed. There are stories of Argiope scouts drawing large numbers of Argiope spiders to take down their prey in what one researcher described as "methodical". Argiope spiders can become confused if their hierarchy is disrupted, which is the only way to dissuade these creatures from their appointed tasks. The red berry trees have proven the most fruitful places for Argiope spiders to congregate because many of their prey cannot resist the taste of the red berries.

### Phoenix

![Phoenix](https://www.peppercarrot.com/data/wiki/medias/img/creature_phoenix.jpg)
* **Type**: Magical birds.
* **Place**: The Valley of Volcanoes
* **Episode**: ep03
* **Description**: Phoenix are large birds with long feathers on the top of their heads. They are made of magma and fire, and shine with a fiery brilliance. They are near-immortal with their regenerative powers. At the end of a phoenix's life they return to the Valley of Volcanoes. When their bodies are no longer able to contain the magma and fire that binds them together they are consumed by the magma and fire and turn into ash. Those ashes sprinkle down into the magma of the volcano, where it reforms the phoenix. Phoenixes in captivity do not thrive because they are unable to regenerate (some folks have tried to capture the phoenix but are left with nothing but inert ashes and charred cages when the phoenix dies). Female phoenixes can have around three eggs per year. It is difficult for the untrained eye to discern between the male and the female phoenix, and usually can only be done at temperatures that are uncomfortable for most beings.  Phoenix tears are highly sought after for Rea poisoning, a condition where one gathers too much Rea and is incapable of expelling it via spells or other means. Rea poisoning manifests as a magical rash that disrupts magic (hence the inability to reduce the amount of Rea). A phoenix will not willingly give up their tears, opting instead to challenge the supplicant to earn their tears. Many have tried; few have succeeded.

### DragonCow

![DragonCow](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragoncow.jpg)
* **Type**: Dragon, female.
* **Place**: Sky over Squirrel's End forest
* **Description**: DragonCows are docile creatures that tend to congregate over the forests of Squirrel's End. They spend most of their days chewing on trees and calmly gliding over all of Hereva. Most creatures in Hereva don't tend to bother DragonCows, partially because of their large size and partially because mature DragonCows have been known to kick their opponents with such force as to break the sound barrier. DragonCows are prized for their milk, with some finding it tasting better than ordinary cow's milk. DragonCows are much more intelligent than they appear and are capable of critical thinking and complex problem solving. Unfortunately DragonCows are a little clumsy during take-off and landing, which is why you'll no doubt be able to locate where a DragonCow has attempted take off / landing by the wreckage of trees and the ruts in the ground.
* **Episode**: ep03

### Fisher Owl

![Fisher Owl](https://www.peppercarrot.com/data/wiki/medias/img/creature_fisher-owl.jpg)
* **Type**: Owl, female.
* **Place**: Squirrel's End forest
* **Description**: The Fisher Owl is a unique creature in the ecology of Hereva. Most Fisher Owls appear as regular owls, and indeed many of them are indistinguishable from other owls. It is only with the presence of a fishing pole that the unique characteristics of the Fisher Owl become apparent. The Fisher Owl notices the fishing pole and immediately creates such a strong bond that it will use that fishing pole to capture its prey. Many times that will result in the owl using a piece of cheese on a hook as bait to lure unsuspecting mice towards it, but it has also resulted in owls using bait for fish, bait for reptiles, and on some occasions bait for humans (there have been no reports of a human taking the bait, however.) This of course has scientists asking how the Fisher Owls acquire little fishing poles, as none have been seen assembling poles from string, twigs, and hooks. The current theory is that poles are passed down from generation to generation though there are theories that someone or something is busily making little poles for Fisher Owls. One day scientists hope to unravel this baffling mystery. Most Fisher Owls have been spotted in the forest of Squirrel's End but there may be other locations where they hunt. It is not inconceivable to find Fisher Owls sitting in frozen areas fishing, but one wonders where they got the tiny drills to puncture the ice.
* **Episode**: ep04

### Savant-Ant

![Savant-Ant](https://www.peppercarrot.com/data/wiki/medias/img/creature_savant-ant.jpg)
* **Type**: ants, neutral.
* **Place**: Squirrel's End, around the house of Pepper
* **Description**: The Savant-Ants are a group of ants that were magically uplifted to genius intelligence by one of Pepper's potions of genius. She thought the potion had failed after testing it on Carrot and threw it out the window onto a colony of unsuspecting ants. These ants have gone on to make several discoveries including mathematics and space travel, as later witnessed by Pepper. Savant Ants look like regular ants save for their incessant need to tell other ants how many advanced degrees they have. They are currently located in Squirrel's End near Pepper's house but with their current ambitions they could be anywhere in Hereva, exploring. Some may even be sneaking into Pepper's House reading the Chaosah Tomes in the hopes of uncovering more secret knowledge for their own experiments.
* **Episode**: ep04, ep19

### Dragon-Drake

![Dragon-Drake](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragon-drake.jpg)
* **Type**: Dragon, male.
* **Place**: Around Komona city, in the sky
* **Description**: Dragon-Drakes are the canonical example of the strange hybrid creatures that came about after The Great Tree of Komona exploded and ripped apart the fabric of magic in Hereva. Many scientists have pondered what caused these creatures to have the head of a duck and the body of a small dragon. What is known about these curious creatures is that they behave more like ducks than dragons, playfully soaring over and around the skies of Komona. They are about the size of a standard duck, save for their long draconic tail. Some have been found floating on water but most times they spend their time in the air. Dragon-Drakes are mostly harmless, but there have been instances of Dragon-Drakes getting attached to various witches flying into the city of Komona, only to be jilted when they are waved away (mostly because the witch can't see in order to navigate the complex landing to get into Komona).
* **Episode**: ep06

### Oversized-Posh-Zombie-Canary

![Oversized-Posh-Zombie-Canary](https://www.peppercarrot.com/data/wiki/medias/img/creature_oversized-posh-zombie-canary.jpg)
* **Type**: Canary, female.
* **Description**: The Oversized-posh-zombie-canary is the unfortunate result of first magical potion contest held in Komona. The canary was first resurrected by Coriander using a Zombiah potion. The canary was then made posh by Saffron's "potion of poshness". Shichimi didn't realize that her potion would be used on the same entity and surmised quickly that the potion of extreme growth that she had concocted would make things worse. Saffron, mistaking Shichimi's reluctance for modesty, poured all of the extreme growth potion on the posh, zombified canary. Shichimi's potion worked better than even she could predict, turning the posh, zombified canary into an oversized, posh, zombified canary. The canary, confused and unsure what was happening, began fluttering about wildly. Naturally this spectacle got the attention of the Komonan guard, who prepared to attack this gargantuan creature. Fortunately Pepper convinced the creature through her, erm, unconventional potion. The bird was last seen sulking off to create a nest, which she guards with quiet dignity.
* **Episode**: ep06

### Old-Sylvan

![Old-Sylvan](https://www.peppercarrot.com/data/wiki/medias/img/creature_old-sylvan.jpg)
* **Type**: Magical tree, male.
* **Place**: Squirrel's End.
* **Description**: Old-Sylvan is a sentient tree that lives in the forest of Squirrel's End. He is part of Pepper's security system. He stands guard, patiently waiting for intruders or other passers-by and detains them. If necessary he will entangle any intruders in a series of vines that grow amongst his branches. He is a jovial creature, happy to have long chats with anyone who happens by but he takes his guard post seriously. He has two branches that he uses as arms and can use them to hold intruders to entangle them. Old-Sylvan is also home to several families of squirrels, who occasionally pop out of his mouth to check to see if there are any intruders nearby. They are curious squirrels and are very good at spotting any trouble that might be coming by Old-Sylvan's way.
* **Episode**: ep09

### Theorem-the-Golem

![Theorem-the-Golem](https://www.peppercarrot.com/data/wiki/medias/img/creature_theorem-the-golem.jpg)
* **Type**: Golem, male.
* **Place**: Moutain behind Squirrel's End.
* **Description**: Theorem the Golem is part of Pepper's security system. He live in the mountains behind Squirrel's End. Most of the time he sits in quiet contemplation. Nobody has asked what he is contemplating during these moments. He sits so still during these moments of contemplation that grass, vines, and moss have grown on him. He even has a nest of pigeons on his left arm. But when an intruder flies by he springs into action. He uses a comically long ball-and-chain to keep his prisoners from getting too far away, but allows them to walk for a bit if they feel like it. He takes great pride in presenting is unwitting captives to Pepper when she remembers to come collect them. Who knows what might happen to them if she didn't check in from time to tine, as Theorem doesn't seem to be proactive in their release. Theorem was recruited by Pepper to help with her security system. As she was wandering around her house she noticed him sitting there in quiet contemplation. When she asked what he was doing he didn't answer her. Mistaking him for a statue she began placing various trap elements around him until she realized that he was following her every move. She then asked again what he was doing. "Thinking" was his reply. She then asked him if he would be willing to help keep intruders out of her house. After a few moments he replied "yes" and that was that. Pepper never had to worry about intruders coming in from the mountains again.
* **Episode**: ep09

### Dragonmoose

![Dragonmoose](https://www.peppercarrot.com/data/wiki/medias/img/creature_dragonmoose.jpg)
* **Type**: Dragon, male.
* **Place**: Clouds, on the top of moutains behind Squirrel's End.
* **Description**: Dragonmoose are the result of a magical fusion of dragons and moose in Hereva. This occurred during the explosion of magic during the collapse of the Great Tree of Komona. Herevan biologists are still trying to work this one out since moose are not native to the universe of Hereva at all. It took some inter-dimensional sleuthing to even determine which creature had suddenly fused with dragons, giving proof for the inter-dimensional theorists that Hereva is connected to other universes while also giving the inter-dimensional skeptics more fuel for their claims that no universe could contain something as ridiculous looking as a moose. Some argue that their reasoning was based on flawed inter-dimensional research, especially the research that claims of a partnership between moose and squirrels. Regardless the dragonmoose still exists in the world of Hereva and can be found in the mountains of Squirrel's End. They have nests with brightly colored eggs where young dragonmoose are hatched. Since the dragonmoose is a relatively young magical concoction little of their habits, let alone ecology has been studied in depth. Several attempts to observe the dragonmoose have been attempted but most, if not all expeditions have found themselves being plucked from their observational spaces and placed gently into the dragonmoose nest, where a bemused dragonmoose nuzzles the observers until they make their escape. Pepper has used this feature of the dragonmoose as part of her security system around her house. 
* **Episode**: ep09

### Bigfish

![Bigfish](https://www.peppercarrot.com/data/wiki/medias/img/creature_bigfish.jpg)
* **Type**: Fish, male.
* **Place**: Sea side.
* **Description**: The Bigfish is just that: a very big fish that lives in the seas of Hereva. It uses its large mouth to eat smaller fish and sea creatures. It can also use its mouth to scrape the small islands of Hereva for any land-based creatures. It spends most of its day eating, storing the results of its fishing in its mouth for days or weeks. It then swallow its catch and begins the slow digestion process. Bigfish are indiscriminate in what they will eat as it can take days for it to suffer the consequences of its meal. The fish does have a giant uvula in its mouth which if attacked can be painful and cause the fish to expel its undigested catch. Most sea creatures have never seen a uvula though so they don't know its purpose.
* **Episode**: ep10

### Spider-Admin

![Spider-Admin](https://www.peppercarrot.com/data/wiki/medias/img/creature_spider-admin.jpg)
* **Type**: Spiders.
* **Place**: In caverns located in forests of Hereva.
* **Description**: Spider-admins are the caretakers and administrators of Hereva's Crystal Ball Network. This network allows folks access to news, videos, entertainment, gossip, and, yes, even the more sundry things in Herevan life. Not everyone in Hereva is connected to the network since the crystal balls can be quite expensive and the costs to maintain a direct wired connection can be quite high. Fortunately there are also public access crystal balls provided in many different locations. Spider-admins make sure the network is operational and also consult with folks wanting to generate sites for folks to browse. Spider-admins are quite large and can seem intimidating at first but they are more interested in helping folks to enjoy the network they created than in intimidation. One set of administrators likes in a cave where Thyme has set up a Crystal Ball Terminal for her own use (she doesn't enjoy sharing with others, and wants the fastest connection available). Spider-admins rarely leave their cave, preferring the glow of the crystal balls they use for development and administration.
Some argue that crystal balls would be cheaper if they did not contain an excessive range of colors that most cannot perceive. The Spider-admin Syndicate documented that various spider species assisted in the recalibration of production. Using their tetrachromatic vision with different spectrums, they were able to increase the basic color palette from 3 to 6. The producers claim that this way all Hereva species can enjoy their crystal balls and that the final price was not affected.
* **Episode**: ep15

### Phanda

![Phanda](https://www.peppercarrot.com/data/wiki/medias/img/creature_phanda.jpg)
* **Type**: Combination of Elephant and Badger
* **Place**: Forests of Hereva (exact location unknown)
* **Description**: The Phanda is a large, furry creature with the face and trunk of an elephant and the body of a badger. It has horns along its back and is considered quite dangerous, especially to unwary travelers in the jungles and forests of Hereva.
* **Episode**: ep17

### Genie of success

![Genie of success](https://www.peppercarrot.com/data/wiki/medias/img/creature_genie_of_success.png)
* **Type**: Genie with a walrus head
* **Place**: In a small magic lamp shaped like a teapot
* **Description**: The Genie of Success is a capricious magical being who shows up when folks are at their lowest. He offers dreams of power, wealth, and glory but at a cost that is hidden within a maze of legalese and contracts. It's unclear how the genie benefits from these transactions but invariably his glowing teapot will drop on unsuspecting folks who feel the world has left them battered and broken with no clear way out. Some claim that the genie has made noble bargains in the past but on closer inspection the terms cost way more than the benefit received. The genie himself is a handsome walrus-faced figure with broad shoulders, chiseled features, and a turban. His skin is yellow with brown stripes. He wears a red vest and golden bands that cover his lower arms. He travels in a teapot, though most times one cannot see it until he is ready to make a deal.

* **Episode**: ep23

### Scorpio

![Shapeshifter](https://www.peppercarrot.com/data/wiki/medias/img/creature_scorpio_alive.jpg)

* **Type**: Scorpion like creature
* **Place**: Castle in episode 38
* **Description**: Scorpio are giant, heavily armored scorpion-like creatures. They have four spiky legs and a scorpion-like tail. They live in nests full of human skulls and bones, which they collect from their unwitting victims. The tail has a giant poisonous stinger which the scorpio uses to subdue its prey. Its heavily armored head has two horned mandibles which it uses to trap prey. Scorpio are used as guardians around treasure. They keep watch and are formidable foes for any adventurers caught in their path.  Scorpio venom does not kill its victims. The venom causes intense pain and incapacitates the victim. If left untreated it can paralyze the victim after several hours. Hippiah magic can remove the venom but requires intense concentration to find the venom and remove it.  It is possible to overpower a Scorpio by surrounding it but most scorpio will use a wall or other barrier as a defense against such attacks. 
* **Episode**: ep38

### Shapeshifter
![Shapeshifter](https://www.peppercarrot.com/data/wiki/medias/img/creature_shapeshifter_transforming.jpg)

* **Type**: Hind-legged monster with long fangs and nails
* **Place**: Castle in episode 38
* **Description**: The Shapeshifter is a class of beings used to guard important items such as treasures or other secret areas. It's so named because of its abilities to transform from a smooth creature into a horned creature. During its transformation it lets out a piercing yell that can crack glass and deafen its opponents. Shapeshifters are magically created creatures, usually created by powerful wizards or witches. They are formidable creatures in their natural state with pale gray skin, large teeth, and huge claws. But when they are enraged they transform into an even larger, deadlier creature with glowing skin and more demonic features. Only a handful of them have been encountered in the wild. Each time the adventurers have barely lived to tell the tale. 
* **Episode**: ep38

## Demons of Chaosah

### Hornuk

![Hornuk](https://www.peppercarrot.com/data/wiki/medias/img/creature_hornuk.jpg)
* **Type**: Demon of Chaosah, male.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: Hornuk is a Chaosah demon with green fur and two prominent horns on his head. He is a fierce demon who not only uses his claws and horns to gore his opponents but also consumes his enemies with his gigantic maw, where they are slowly digested over hundreds of years. He is not as magically gifted as the other Chaosah demons but he doesn't have to be. He will relentlessly pursue his opponents until they either give up or are destroyed; whichever comes first.
* **Episode**: ep08

### Eyeük

![Eyeük](https://www.peppercarrot.com/data/wiki/medias/img/creature_eyeuk.jpg)
* **Type**: Demon of Chaosah, male.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: Eyeuk is the leader of the Chaosah demons. He has red fur, four eyes, and a trident that he wields. He has three horns on his head. As the leader of the of the Chaosah demons he thinks more strategically than the other demons. He also uses his eyes to survey get a sense of what dangers might be lurking ahead. He uses his trident as a weapon and skillfully spears his opposition before they have time to react. When attacking he slows down time in order to react more quickly to his foes. He possesses the ability to cast Chaosah spells but has been locked away from traversing into other universes unless summoned; a restriction he is desperate to break. One must be careful around the clever Eyeuk, who will try to release this restriction at every turn.
* **Episode**: ep08

### Spidük

![Spidük](https://www.peppercarrot.com/data/wiki/medias/img/creature_spiduk.jpg)
* **Type**: Demon of Chaosah, female.
* **Place**: Dimension Chaosah, can be summoned for a short time to Hereva's main reality.
* **Description**: Spiduk is a Chaosah demon. She looks similar to a large purple spider. She is also skilled at Chaosah magic but lacks the ability to travel between universes (much like Eyeuk). Unlike Eyeuk though this restriction doesn't bother Spiduk, who is contented with her service to the Chaosah witches. She is most comfortable on the battlefield where she will attack several opponents at the same time using her many arms and webbing. She has also mastered Chaosah battle magic which allows her to alter reality to her choosing and gives her formidable powers. She is not to be taken lightly and many foes have learned the hard way not to cross her path. However she also appreciates the finer things in life like a nice cup of tea and will stop her battle rages to indulge a bit in the finer things like pastries, cakes, and the like before destroying her enemies.
* **Episode**: ep08

## Dragons of Hereva

Dragons roamed the lands of Hereva for as long as any memory can recall. The time they aren't soaring over Hereva's landscape they spend guarding their lairs. They are capricious beings, with their own desires and agendas. There is even a myth that the sun of Hereva is comprised of two dragons fighting a continual battle and launching great bursts of flame at each other. Most dragons do not elaborate on this myth, and will quickly change the subject should it come up.

They regard humans as curiosities and are fascinated that beings with such short lifespans are constantly inquiring about events they can't possibly know.

There is a type of dragon known as the Petite Dragon that has been known to be around members of Ah. Whether these can be considered "true" dragons or another species on Hereva has yet to be determined. After a number of years the members of Ah release these Petite Dragons into the wild, never to see them again. It is difficult to keep track of dragons in the wild as magical tagging never seems to stick on them for long. Some believe the dragons seen with members of Ah are actually young dragons, and the reason they often return to the land of the setting moons is because that is their home. Whatever the case, the dragons and members of Ah will not elaborate on their relationship.

Some dragons become magically bonded to members of Ah, allowing them to be ridden as fearsome mounts. One can only wonder how such pairings are achieved since the dragons still maintain their autonomy and draconic ways.

Near the end of a dragon's life, it will head toward the three setting moons, near the temples of Ah. There it will spend the rest of it's days in quiet rest and contemplation. The members of Ah are respectful and do not disturb any of the dragons in the area unless the dragon initiates conversation. To disturb a dragon in these final days is considered anathema, and carries stiff punishments.

## Dragons and Hereva's Pre-History

The Dragons of Hereva have lived in the land of Hereva long before human recollection. It wasn't until The Great Tree of Komona that the dragons took notice of the humans in Hereva. Whether that was because the humans didn't arrive until The Great Tree of Komona appeared is something the Komonan scholars wrestle to resolve. Many conversations with the dragons prove inconsistent at best and obfuscated at worst. One theory is that the dragons simply did not pay any attention to the humans until they wielded magic, much in the same way that humans don't pay attention to the ants until they start building superstructures in their back yard. Another theory is the dragons purposefully obfuscate human history, answering in riddles and conflicting details in the hopes of keeping humans away from some hidden lore.

### Red Dragon (Fire Dragon)

![Red Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_red-dragon.jpg)
* **Type**: Dragon
* **Place**: Caves
* **Description**: Red Dragons are reddish, scaly dragons. They have a bony neck frill that is protected by eight horns on the dragon's head; two on top and three horns on each side. This frill also has horns protruding from it. Red Dragons are temperamental, even by dragon standards. They live in the cave systems of Hereva, where they can amass large treasure hordes of gold. Red Dragons protect their lairs with their fiery breath, along with their wings, claws, and horns. Red Dragons spend most of their days sleeping near or on their hordes. They do not suffer trespassers or fools who enter their lairs, choosing instead to attack with their breath upon discovery. Should the intruder persist the dragon will exert more effort to expel the intruder. Few Herevan scientists have managed to study Red Dragons, opting instead for less temperamental creatures like the Dragon Cow instead. Red Dragons are quite intelligent and have been known to have long conversations with those they tolerate but getting to that point is quite the challenge.
* **Episode**: ep14

### Air Dragon

![Air Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_air-dragon.jpg)
* **Type**: Dragon
* **Place**: Flying above Hereva
* **Description**: Air Dragons are dragons that spend most of their time flying over Hereva. They are greenish in color and are optimized for flight. They have webbing on the sides of their long, aerodynamic heads. It is rare to see an Air Dragon not in flight, and any Air Dragon not in flight is probably wounded or exhausted. They are similar to dragon cows in their penchant for tearing off the tops of trees or other flora in Hereva, though some have been known to pick up livestock if hungry enough. Air Dragons are a rare sight, and seeing more than a few of them in flight is considered good luck.
* **Episode**: ep14

### Swamp Dragon

![Swamp Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_swamp-dragon.jpg)
* **Type**: Dragon
* **Place**: In the Swamps of Hereva
* **Description**: Swamp Dragons are dragons that live in the swamps of Hereva. Their composition is curious, as it is unclear if they are dragons that prefer the cooling mud of the swamp or magical constructs made of mud that look like dragons. Nobody has ever seen a swamp dragon outside of the swamp so the question remains. They have a playful nature, but if threatened they can project a massive spray of mud at any attackers. This spray has proven a strong deterrent to Herevan scientists who have tried to answer the question of the dragon's composition. Swamp Dragons don't mind visitors and will chat for hours with anyone who wanders through into their swamp. They are not as territorial as Red Dragons, but they still have a territorial instinct that can prompt them to perceive a visitor as a threat (though they will at least give ample warning before unleashing their mud-spray).
* **Episode**: ep14

### Lightning Dragon

![Lightning Dragon](https://www.peppercarrot.com/data/wiki/medias/img/creature_lightning-dragon.jpg)
* **Type**: Dragon
* **Place**: In the Mountains of Hereva
* **Description**: Lightning Dragons are elemental creatures made of pure energy. Most of the time they can be found in their fearsome dragon-form but they sometimes change into other forms. Lightning Dragons are more elemental than dragon, however for reasons of taxonomy and because early Hereva Scientists saw more dragon characteristics rather than elemental characteristics the Lightning Dragon is classified as a dragon. Like the swamp dragon they tend to be more curious than aggressive and take great pleasure in their occasional visitors. Unfortunately the visitors of Lightning Dragons can find the experience a bit shocking if they're not well grounded. In their dragon for the Lightning Dragon takes the appearance of a large dragon with horns on either side of its beak and large teeth. When provoked the Lightning Dragon prefers to stay in its dragon form, using its beak to deliver charged bites to its opponent. Touching a Lightning Dragon can provide a nasty electrical shock, as the dragon itself is pure electrical energy. Herevan scientists have repeatedly tried to find the source of the Lightning Dragon's power but each time they tried the dragon overloaded their instruments. Lightning Dragons will chat with their visitors and offer both insight and wisdom to those who seek it. They are kind and patient, even with those who are obviously ill-prepared for their lessons.
* **Episode**: ep14
